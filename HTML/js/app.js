class Properties extends React.Component {
  
    constructor(props) {
      super(props);
      this.state = {
        properties: { Designer: [], Client: [], ITI: [], RTAM: [] },
        batchProperties: props.batchProperties,
        properties_file: 'properties.' + props.batchProperties.version + '.json',
        batchTemplate:{
          init:'',
          closure:'',
          client:'',
          clientUnique:'',
          designer: '',
          designerUnique: '',
          iti:'',
          rtam:'',
          hotfixes:'',
          hotfixesUnique:''
        },
        batchScript:false,
        hotfixes:[{ id:1, value:''}]
      };

    }    

    // Initial AJAX requests
    componentDidMount() {

      $.ajax({
        url: 'lib/' + this.state.properties_file,
        dataType: 'json',
        cache: false,
        success: function(data){
          this.setState({ properties:data })
        }.bind(this)
      });

      // Init template
      $.ajax({
        url: 'batch_templates/init.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              init:data
            }
          });
        }.bind(this)
      });

      // Closure template
      $.ajax({
        url: 'batch_templates/closure.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              closure:data
            }
          });
        }.bind(this)
      });

      // Client template
      $.ajax({
        url: 'batch_templates/client.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              client:data
            }
          });
        }.bind(this)
      });

      // Client Unique Architecture Template
      $.ajax({
        url: 'batch_templates/clientUniqueArch.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              clientUnique:data
            }
          });
        }.bind(this)
      });

      // Designer template
      $.ajax({
        url: 'batch_templates/designer.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              designer:data
            }
          });
        }.bind(this)
      });

      // Designer Unique Architecture Template
      $.ajax({
        url: 'batch_templates/designerUniqueArch.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              designerUnique:data
            }
          });
        }.bind(this)
      });

      // ITI template
      $.ajax({
        url: 'batch_templates/iti.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              iti:data
            }
          });
        }.bind(this)
      });

      // RTAM template
      $.ajax({
        url: 'batch_templates/rtam.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              rtam:data
            }
          });
        }.bind(this)
      });

      // Hotfixes template
      $.ajax({
        url: 'batch_templates/hotfixes.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              hotfixes:data
            }
          });
        }.bind(this)
      });

      // Hotfixes Unique Architecture Template
      $.ajax({
        url: 'batch_templates/hotfixesUniqueArch.txt',
        dataType: 'text',
        cache: false,
        success: function(data){
          this.setState({
            batchTemplate : {
              ...this.state.batchTemplate,
              hotfixesUnique:data
            }
          });
        }.bind(this)
      });

    }

    handleSubmit = e => {
      e.preventDefault();
      
      const formData = new FormData(e.target);

      var formObject = {};
      var object = {};

      formObject[this.state.batchProperties.software] = new Array;

      if( this.state.batchProperties.include_iti )
        formObject['ITI'] = new Array;

      if( this.state.batchProperties.include_rtam )
        formObject['RTAM'] = new Array;
      
      formData.forEach(function(value, key){
        var regex = /\[(.*?)\]/g; // gets whats between square brackets
        var match = regex.exec(key);

        if( match ){
          var property = match[1];
          var type = key.replace(match[0],'');

          formObject[type].push({ 
            property: property,
            value: value
          });
        }
        
      });
      

      this.generateBatch(formObject);
    }

    generateBatch = formValues => {

      var batchSoftwareProperties = '';

      // Client / Designer properties
      for( var x in  formValues[this.state.batchProperties.software] ){
          
          var formProperty = formValues[this.state.batchProperties.software][x].property;
          var formValue = formValues[this.state.batchProperties.software][x].value;
          
          var sourceProperty = this.state.properties[this.state.batchProperties.software].find(function(element) {
            return element.property == formProperty;
          });
          
          // checks if there was a change from default value
          if( sourceProperty.control.default != formValue ){
            batchSoftwareProperties += formProperty + '=' + '"'+ formValue + '"' + ' ';
          }
      }

      batchSoftwareProperties = batchSoftwareProperties.trim();

      // ITI properties
      if( this.state.batchProperties.include_iti ){

        var batchITIProperties = '';

        for( var x in  formValues.ITI ){
          
          var formProperty = formValues.ITI[x].property;
          var formValue = formValues.ITI[x].value;
          
          var sourceProperty = this.state.properties.ITI.find(function(element) {
            return element.property == formProperty;
          });
          
          // checks if there was a change from default value
          if( sourceProperty.control.default != formValue ){
            batchITIProperties += formProperty + '=' + '"'+ formValue + '"' + ' ';
          }
        }

        batchITIProperties = batchITIProperties.trim();
      }

      // RTAM properties
      if( this.state.batchProperties.include_rtam ){

        var batchRTAMProperties = '';

        for( var x in  formValues.RTAM ){
          
          var formProperty = formValues.RTAM[x].property;
          var formValue = formValues.RTAM[x].value;
          
          var sourceProperty = this.state.properties.RTAM.find(function(element) {
            return element.property == formProperty;
          });
          
          // checks if there was a change from default value
          if( sourceProperty.control.default != formValue ){
            batchRTAMProperties += formProperty + '=' + '"'+ formValue + '"' + ' ';
          }
        }

        batchRTAMProperties = batchRTAMProperties.trim();
      }

      var batchScript = this.state.batchTemplate.init;
      
      // Software batch preparation
      if( this.state.batchProperties.software == 'Client' ){

        var softwareBatch = this.state.batchProperties.architecture == '6432' ? 
        this.state.batchTemplate.client : this.state.batchTemplate.clientUnique;

      } else {

        var softwareBatch = this.state.batchProperties.architecture == '6432' ? 
        this.state.batchTemplate.designer : this.state.batchTemplate.designerUnique;
      }
      

      if( this.state.batchProperties.architecture == '6432' ){
        
        softwareBatch = softwareBatch.replace('{msi}', this.state.batchProperties.msi);
        softwareBatch = replaceAll(softwareBatch, '{msi64}', this.state.batchProperties.msi64 );
        
      }

      if( this.state.batchProperties.architecture == '64' ){ 
        softwareBatch = replaceAll(softwareBatch, '{msi}', this.state.batchProperties.msi64 );
      }

      if( this.state.batchProperties.architecture == '32' ){
        softwareBatch = replaceAll(softwareBatch, '{msi}', this.state.batchProperties.msi );
      }

      softwareBatch = softwareBatch.replace('{properties}', batchSoftwareProperties);

      batchScript += softwareBatch;

      // ITI batch preparation
      if( this.state.batchProperties.include_iti ){
        var itiBatch = this.state.batchTemplate.iti;

        itiBatch = itiBatch.replace('{msi}',this.state.batchProperties.iti);
        itiBatch = itiBatch.replace('{properties}',batchITIProperties);

        batchScript += "\n\n" + itiBatch;
      }

      // RTAM batch preparation
      if( this.state.batchProperties.include_rtam ){
        var rtamBatch = this.state.batchTemplate.rtam;

        rtamBatch = rtamBatch.replace('{msi}',this.state.batchProperties.rtam);
        rtamBatch = rtamBatch.replace('{properties}',batchRTAMProperties);

        batchScript += "\n\n" + rtamBatch;
      }

      if( this.state.batchProperties.include_hotfixes === false ){
        batchScript += "\n\n" + "goto :success";
      } else{

        // Include HotFixes

        if( this.state.batchProperties.architecture == '6432' ){
          batchScript += "\n\n" + this.state.batchTemplate.hotfixes;
          
          var software = this.state.batchProperties.software;

          // 32 BITs Hotfixes
          batchScript +="\n\n:x86";
          batchScript +="\nrem Applies patch to x86 Windows Operating System";
            
          for ( var x in this.state.hotfixes ){
            var fileName = this.state.hotfixes[x].value;

            batchScript +="\n\necho Copying file " +  fileName + " to C:\\Program Files\\NICE Systems\\Real-Time " + software + "\\";
            batchScript +='\nxcopy  ".\\Hotfixes\\' + fileName + '" "C:\\Program Files\\NICE Systems\\Real-Time ' + software + '\\*" /y';
            
          }

          batchScript += "\n" + "goto :success";

          // 64 BITs Hotfixes
          batchScript +="\n\n:x64";
          batchScript +="\nrem Applies patch to x64 Windows Operating System";
            
          for ( var x in this.state.hotfixes ){
            var fileName = this.state.hotfixes[x].value;

            batchScript +="\n\necho Copying file " +  fileName + " to C:\\Program Files (x86)\\NICE Systems\\Real-Time " + software + "\\";
            batchScript +='\nxcopy  ".\\Hotfixes\\' + fileName + '" "C:\\Program Files (x86)\\NICE Systems\\Real-Time ' + software + '\\*" /y';
            
          }

          batchScript += "\n" + "goto :success";

        } // end 6432

        if( this.state.batchProperties.architecture == '32' ){
          batchScript += "\n\n" + this.state.batchTemplate.hotfixesUnique;
          
          var software = this.state.batchProperties.software;

          // 32 BITs Hotfixes
          batchScript +="\n\n:ApplyHotfixes";
          batchScript +="\nrem Applies patch to x86 Windows Operating System";
            
          for ( var x in this.state.hotfixes ){
            var fileName = this.state.hotfixes[x].value;

            batchScript +="\n\necho Copying file " +  fileName + " to C:\\Program Files\\NICE Systems\\Real-Time " + software + "\\";
            batchScript +='\nxcopy  ".\\Hotfixes\\' + fileName + '" "C:\\Program Files\\NICE Systems\\Real-Time ' + software + '\\*" /y';
            
          }

          batchScript += "\n" + "goto :success";

        } // end 32

        if( this.state.batchProperties.architecture == '64' ){
          batchScript += "\n\n" + this.state.batchTemplate.hotfixesUnique;
          
          var software = this.state.batchProperties.software;

          // 64 BITs Hotfixes
          batchScript +="\n\n:ApplyHotfixes";
          batchScript +="\nrem Applies patch to x64 Windows Operating System";
            
          for ( var x in this.state.hotfixes ){
            var fileName = this.state.hotfixes[x].value;

            batchScript +="\n\necho Copying file " +  fileName + " to C:\\Program Files (x86)\\NICE Systems\\Real-Time " + software + "\\";
            batchScript +='\nxcopy  ".\\Hotfixes\\' + fileName + '" "C:\\Program Files (x86)\\NICE Systems\\Real-Time ' + software + '\\*" /y';
            
          }

          batchScript += "\n" + "goto :success";

        } // end 64
        
      } // end include hotfixes
      
      batchScript += "\n\n" + this.state.batchTemplate.closure;


      this.setState({ batchScript:batchScript });

      jump('batchScriptContainer');
      
      /*console.log(batchSoftwareProperties);
      console.log(batchITIProperties);
      console.log(batchRTAMProperties);*/
      
    }

    downloadBatchScript = () =>{
      download('batchScript.bat', this.state.batchScript );
    }

    addHotFix = () =>{

      var newHotfix = { id:this.state.hotfixes.length +1 , value:''};

      this.setState({ hotfixes : [
          ...this.state.hotfixes,
          newHotfix
      ]})
    }

    setHotfix = (value, hotfixId) => {

      this.setState({
        hotfixes: this.state.hotfixes.map( hotfix => {
          if( hotfixId === hotfix.id ){
            return{
              ...hotfix,
              value
            }
          }
          return hotfix;
        })
      });
    }

    render(){
      return (
      <div className="props-conatiner">
        <h2>Installation Summary</h2>
        <p>You're installing...</p>
        <ul>
          <li>RT {this.state.batchProperties.software} version: {this.state.batchProperties.version} - MSI: {this.state.batchProperties.msi} </li>
          <li>{this.state.batchProperties.include_iti ? 'Installing ITI Bridge: ' + this.state.batchProperties.iti : 'Not Including ITI Bridge' }</li>
          <li>{this.state.batchProperties.include_rtam ? 'Installing RTAM: ' + this.state.batchProperties.rtam : 'Not Including RTAM' }</li>
        </ul>
        <h2>{this.state.batchProperties.software == 'Client' ? 'Client Properties':'Designer Properties'}</h2>
            <form onSubmit={this.handleSubmit} class="properties-form" >
              <table class="table table-bordered">
                <thead>
                  <tr class="d-flex" >
                    <th class="col-3" >Property</th>
                    <th class="col-2" >Values</th>
                    <th class="col-5" >Default value</th>
                    <th class="col-2" >Description</th>
                  </tr>
                </thead>
                <tbody>
                  <SoftwareProperties 
                    properties={this.state.properties} 
                    software={this.state.batchProperties.software}
                     />
                </tbody>
              </table>
              {this.state.batchProperties.include_iti ? (
                <div classname="iti-properties">
                  <h2>ITI Bridge Properties</h2>
                  <table class="table table-bordered">
                    <thead>
                      <tr class="d-flex" >
                        <th class="col-3" >Property</th>
                        <th class="col-2" >Values</th>
                        <th class="col-5" >Default value</th>
                        <th class="col-2" >Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <ITIProperties  properties={this.state.properties.ITI} />
                    </tbody>
                  </table>
                </div>
                
              ) : ('')}

              {this.state.batchProperties.include_rtam ? (
                <div classname="rtam-properties">
                  <h2>RTAM Properties</h2>
                  <table class="table table-bordered">
                    <thead>
                      <tr class="d-flex" >
                        <th class="col-3" >Property</th>
                        <th class="col-2" >Values</th>
                        <th class="col-5" >Default value</th>
                        <th class="col-2" >Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <RTAMProperties  properties={this.state.properties.RTAM} />
                    </tbody>
                  </table>
                </div>
                
              ) : ('')}

              {this.state.batchProperties.include_hotfixes ?(
              <div className="hot-fixes-wrapper">
                <h2>Hot Fixes</h2>
                <p>Single files that will be copied to the RT {this.state.batchProperties.software} program files folder</p>
                {this.state.hotfixes.map((key,index) =>{
                  return (
                    <div className="form-group row">
                      <label for="msi" className="col-2 col-form-label">Hotfix {key.id} file name:</label> 
                      <div className="col-10">
                        <input required type ="text" value={key.value} onChange={ e => this.setHotfix(e.target.value, key.id)} className="form-control" />
                      </div>
                    </div>
                )
                })}
                <div>
                  <button type="button" onClick={this.addHotFix} className="btn btn-info" >Add Hot Fix</button>
                </div>
              </div>
              ) : ('')}
              

              <div className="form-group row">
                <div className="offset-4 col-8">
                  <button type="submit" className="btn btn-primary">Generate Script</button>
                </div>
              </div>
            </form>
            <div id="batchScriptContainer">
            {this.state.batchScript ? (
                <div>
                  <h2>Batch Script: <button onClick={this.downloadBatchScript} type="button" className="btn btn-success" >Download file</button></h2>
                  <p>Bear in mind before execution...</p>
                    <ul>
                      <li>All installers must be at the same folder level of the .bat script</li>
                      <li>HotFixes must be placed under a folder named "Hotfixes"</li>
                    </ul>
                  <p>Preview:</p>
                  <pre id="batchScript" >
                    {this.state.batchScript}
                  </pre>
                </div>
              ) : ('')}
            </div>
      </div>
      );
    }

    
  } // class end

  function SoftwareProperties(props){

    var properties = props.properties;
    var fullProperties = null;
    
    fullProperties = props.software == 'Client' ? properties.Client : properties.Designer;
    
    return( renderTableCols(fullProperties, props.software) );
  }
  
  function RTAMProperties(props){
    return( renderTableCols(props.properties, 'RTAM') );
  }

  function ITIProperties(props){
    return( renderTableCols(props.properties, 'ITI') );
  }

  function renderTableCols(data, type){

    try{
      var htmlElements = data.map((key,index) => {
        
          var input_name = type + '[' + key.property + ']';  

          if( key.control.type =='select'){
            return(
              <tr className="d-flex">
                <th className="col-3">{key.property}</th>
                <td className="col-2" ><PropertyValues values={key.values} /></td>
                <td className="col-5" >
                    <select className="custom-select" name={input_name} >
                      <RenderOptions options={key.control.values} default={key.control.default} />
                    </select>
                </td>
                <td className="col-2" >{key.description}</td>
              </tr>
            );
          }
          
          if( key.control.type =='text'){

            var input_name = type + '[' + key.property + ']';

            return (
              <tr className="d-flex" >
                <th className="col-3">{key.property}</th>
                <td className="col-2" > <PropertyValues values={key.values} /> </td>
                <td className="col-5" >
                  <input type="text" name={input_name} className="form-control"  defaultValue={key.control.default} />
                </td>
                <td className="col-2" >{key.description}</td>
              </tr>
            );
          }
          
          if( key.control.type =='checkbox'){
            return (
              <tr className="d-flex" >
              <th className="col-3">{key.property}</th>
              <td className="col-2" ><PropertyValues values={key.values} /></td>
              <td className="col-5" >
                  <RenderCheckBox type={type} property={key.property} value={key.control.value} default={key.control.default} />
              </td>
              <td className="col-2" >{key.description}</td>
            </tr>
            );
          }
        });
    }
    catch(e){
      console.log(e);
      console.log(properties);
    }

    return htmlElements;
  }

  function PropertyValues(props) {

    const propertyValues = props.values.map((key, index) => { 

    return ( <div>{key}</div> );
      
    });

    return propertyValues;
  }

  function RenderOptions(props) {

    const defaultValue = props.default;
    const options = props.options.map((key, index) => { 


        if( defaultValue == Object.values(key)[0] ){
          return ( 
            <option selected value={ Object.values(key)[0] } >{ Object.keys(key)[0] }</option>   
          );
        } else {
          return ( 
            <option value={ Object.values(key)[0] } >{ Object.keys(key)[0] }</option>   
          );
        }
    });

    return options;
  }

  function RenderCheckBox(props) {

    const defaultValue = props.default;
    const checkValue = props.value;

    var input_name = props.type + '[' + props.property + ']';

    if( defaultValue == checkValue ){
      var input = (<input defaultChecked name={input_name} type="checkbox" class="custom-control-input" value={checkValue} />);
    } else {
      var input = (<input name={input_name} type="checkbox" class="custom-control-input" value={checkValue} />);
    }

    return( <label class="custom-control custom-checkbox">
              {input}
              <span class="custom-control-indicator"></span> 
              <span class="custom-control-description"></span>
            </label>
    );
  }

 function jump(h){
   var top = document.getElementById(h).offsetTop;

   setTimeout(function(){
    window.scrollTo(0,top);
   },100);

 }

 function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}

// Form Class

class Form extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      software : "Client",
      version: "6.7",
      msi : "NICE Real-Time Client.msi",
      msi64: "NICE Real-Time Client 64.msi",
      include_iti : false,
      iti:"",
      include_rtam : false,
      rtam:"",
      include_hotfixes : false,
      architecture:"6432",
      show_form: true
    };
  }

  setSoftware = e =>
    this.setState({ software:e.target.value });

  setVersion = e =>
    this.setState({ version:e.target.value });

  setMsi = e => 
    this.setState({ msi:e.target.value });

  setMsi64 = e => 
    this.setState({ msi64:e.target.value });    

  toggleITI = () =>
    this.setState({ include_iti: !this.state.include_iti });

  setITI = e => 
    this.setState({ iti:e.target.value });
  
  toggleRTAM = () =>
    this.setState({ include_rtam: !this.state.include_rtam });
  
  toggleHotFixes = () =>
    this.setState({ include_hotfixes: !this.state.include_hotfixes });

  setRTAM = e => 
    this.setState({ rtam:e.target.value });

  setArchitecture = e => 
    this.setState({ architecture:e.target.value });
  
  formSubmit = e =>{
    e.preventDefault();
    this.setState({ show_form:false });
    RenderProperties( this.state );
  }
    
  render(){

    if( this.state.show_form === true ){

      return(
        <form onSubmit={this.formSubmit}  >
              <div className="form-group row">
                <label for="software" className="col-4 col-form-label">Software to install</label> 
                <div className="col-8">
                  <select onChange={this.setSoftware} required="required" className="custom-select">
                    <option selected value="Client">RT Client</option>
                    <option value="Designer">RT Designer</option>
                  </select>
                </div>
              </div>
              <div className="form-group row">
                <label for="softwareVersion" className="col-4 col-form-label">Version</label> 
                <div className="col-8">
                  <select onChange={this.setVersion} required="required" className="custom-select">
                    <option value="6.4">6.4</option>
                    <option value="6.5">6.5</option>
                    <option value="6.6">6.6</option>
                    <option selected value="6.7">6.7</option>
                  </select>
                </div>
              </div>
              <div className="form-group row">
                <label className="col-4">Architecture Support</label> 
                <div className="col-8">
                  <label className="custom-control custom-radio">
                    <input name="architecture" checked={this.state.architecture == '6432'} onChange={this.setArchitecture} type="radio" className="custom-control-input" value="6432" /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description">Both 64/32 bit</span>
                  </label>
                  <label className="custom-control custom-radio">
                    <input name="architecture" checked={this.state.architecture == '64'} onChange={this.setArchitecture} type="radio" className="custom-control-input" value="64" /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description">64 bit only</span>
                  </label>
                  <label className="custom-control custom-radio">
                    <input name="architecture" checked={this.state.architecture == '32'} onChange={this.setArchitecture} type="radio" className="custom-control-input" value="32" /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description">32 bit only</span>
                  </label>
                </div>
              </div>
              { this.state.architecture == '6432' || this.state.architecture == '32' ?
              (<div className="form-group row">
                  <label for="msi" className="col-4 col-form-label">MSI file name</label>
                  <div className="col-8">
                    <input id="msi" required value={this.state.msi} onChange={this.setMsi} placeholder="example: NICE Real-Time Client.msi" type="text" className="form-control here" />
                  </div>
              </div>) : ('')
              }
              { this.state.architecture == '6432' || this.state.architecture == '64' ?
              (<div className="form-group row">
                  <label for="msi" className="col-4 col-form-label">MSI 64 bit file name</label>
                  <div className="col-8">
                    <input id="msi64" required value={this.state.msi64} onChange={this.setMsi64} placeholder="example: NICE Real-Time Client 64.msi" type="text" className="form-control here" />
                  </div>
              </div>) : ('')
              }
              <div className="form-group row">
                <label className="col-4">Include ITI Bridge</label> 
                <div className="col-8 extra-component-wrapper">
                  <label className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" checked={this.state.include_iti} onChange={this.toggleITI} /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description"></span>
                  </label>
                  <input required disabled={!this.state.include_iti} onChange={this.setITI} placeholder="example: NICE Insight to Impact Bridge for NEP6.4.msi" type="text" className="form-control here" />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-4">Include RTAM</label> 
                <div className="col-8 extra-component-wrapper">
                  <label className="custom-control custom-checkbox">
                    <input  type="checkbox" className="custom-control-input" checked={this.state.include_rtam} onChange={this.toggleRTAM} /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description"></span>
                  </label>
                  <input required disabled={!this.state.include_rtam} onChange={this.setRTAM} placeholder="example: NICE RTAM Upgrade R6.4.0.msi" type="text" className="form-control here" />
                </div>
              </div>
              <div className="form-group row">
                <label className="col-4">Include Hot Fixes</label> 
                <div className="col-8 extra-component-wrapper">
                  <label className="custom-control custom-checkbox">
                    <input  type="checkbox" className="custom-control-input" checked={this.state.include_hotfixes} onChange={this.toggleHotFixes} /> 
                    <span className="custom-control-indicator"></span> 
                    <span className="custom-control-description"></span>
                  </label>
                </div>
              </div>
              <div className="form-group row">
                <div className="offset-4 col-8">
                  <button id="setValues" type="submit" className="btn btn-primary">Continue</button>
                </div>
              </div>
            </form>
      );
    } else {
      return null;
    }
  }
}
  
  // ========================================

   ReactDOM.render(
    <Form />,
    document.getElementById('formContainer')
  );
  
  function RenderProperties(formValues){

    ReactDOM.render(
      <Properties  batchProperties={formValues} />,
      document.getElementById('PropertiesContainer')
    );
  }